import React, { Component } from 'react';
import speaker_1 from "./images/speaker_1.png";
import speaker_2 from "./images/speaker_2.png";
import speaker_3 from "./images/speaker_3.png";
import speaker_4 from "./images/speaker_4.png";
import './speaker.css';

export default class Speaker extends Component {
    constructor(props) {
        super(props);
        this.state = {
        names: [],
        images: [speaker_1, speaker_2, speaker_3, speaker_4],
        count:0
        };
    }

    componentDidMount = () => {
        fetch("names.json")
        .then(response => {
            return response.json();
        })
        .then( result => {
            console.log(result);
            this.setState({
                names: result.names
            });    
        })
    }

    add_speaker = () => {
        const { names } = this.state;
        var name = prompt("名前を入力してください");
        this.setState({
            names: names.concat(name)
        });
    }

    render = () => {
        const { names,images } = this.state;
        var items = [];

        for(let i=0;i < names.length;i++){
            var name = names[i];
            var image = images[i];
            items.push(
                <li>
                    <div className="item">
                        <div className="speaker_img">
                            <img src={ image } alt="speaker"/>
                        </div>
                        <p>{ name }</p>
                    </div>
                </li>
            );
        }

        return (<div>
            <header><h1>Speaker Identification</h1></header>

            <div className="container">
                <ul>{ items }</ul>
            </div>
            <div className="buttons">
                <button onClick={ this.add_speaker } className="reg">登録</button>
                <button className="rec">録音</button>
            </div>
            <div className="buttons">
                <button className="pred">推定</button>
            </div>
        </div>);
    }
}

